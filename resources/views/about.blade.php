@extends('layouts.template')

@section('content')
		
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-left-padding no-right-padding" style="background-image:url('/assets/uploadedimages/g11.jpg')">
				<!-- Container -->
				<div class="container">
					<div class="page-banner-content">
						<h3> </h3>
					</div>
					<div class="banner-content">
						<ol class="breadcrumb">
							<li><a href="index.html">Home</a></li>
							<li class="active">About Us</li>
						</ol>
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Banner -->
			
			<!-- About Section -->
			<div class="about-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<div class="about-content">
								<h5><?php echo $cms[0]->content01;?></h5>
								<?php echo $cms[0]->content02;/*?>
<p>At Zena Dental,we are a different kind of dental practice that combines the latest in dental technology with a kind,warm and welcoming atmosphere.</p>
<p>We are proud to offer high-tech state of the art dental equipment. ranging from Digital X-Rays,Intra Oral Camera,Cavitron,Rotary Endodontics and much more.</p>
								<?php /*
								<p>Zena Dental combines the latest advancements in dental technology with a kind, warm, and welcoming atmosphere. You’ll love the spa-like feel of our office, personal one-on-one care, and our commitment to your health and your smile.</p>
								<p>We're proud to offer high-tech, high-touch dental care that will maximize your comfort and provide minimally invasive treatments with amazing results.</p>
								<p>We're committed to creating brighter smiles, cleaner teeth and gums, and healthier mouths and bodies.</p>
								*/ ?>
							</div>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12 about-img">
							<img src="/assets/uploadedimages/about.PNG" alt="about" title="" style="padding: 5px; border: 1px solid #00d2eb;" />
							<?php /*<img src="/assets/uploadedimages/IMG_8515.JPG" alt="about" title="" style="padding: 5px; border: 1px solid #00d2eb;" />*/?>
							<img src="/assets/uploadedimages/IMG_8517.JPG" alt="about" title="" style="padding: 5px; border: 1px solid #00d2eb;" />
						</div>
					</div>
				</div><!-- Container /- --> 
			</div><!-- About Section -->
			
			<div id="team-section" class="team-section container-fluid no-left-padding no-right-padding">
				<div class="container">
					<div class="section-header">
						<h3>Location</h3>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3361.5449141159497!2d-96.93812778482267!3d32.591658781028364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e8d9376058d1d%3A0xce0f7030d014004!2s445+FM1382+%236%2C+Cedar+Hill%2C+TX+75104%2C+USA!5e0!3m2!1sen!2sin!4v1562171221600!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			
			
			
			
			<!-- Testimonial Section -->
			<div class="testimonial-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Testimonials</h3>
					</div><!-- Section Header /- -->
				</div>
				<div class="testimonial-slider">	
					<?php
						$i = 0;
						while($i<3){$i++;
						foreach($testimonials as $val){
					?>

					<div class="testimonial-box">
						<div class="testimonial-content">
							<i><img src="/assets/uploadedimages/<?php echo $val->imgname;?>" alt="testimonial"/></i>
							<?php echo $val->image_content;?>
						</div>
					</div>
					<?php
						}
						}
					?>				
					
				</div>
			</div><!-- Testimonial Section /- -->
			
			
			
@endsection