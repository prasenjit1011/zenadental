@extends('layouts.template')
@section('content')
<style>
<?php /*li {
  background: url("arrow.png") 0 50% no-repeat;
  list-style-type: none;
  padding-left: 12px;
}*/?>
</style>
			<!-- Slider Section -->
			<div id="home-revslider" class="slider-section container-fluid no-padding">
				<!-- START REVOLUTION SLIDER 5.0 -->
				<div class="rev_slider_wrapper">
					<div id="home-slider1" class="rev_slider" data-version="5.3">
						<ul>
							<?php
								foreach($banner as $val){
									if(!empty($_REQUEST['xyzpp'])){
										$val->imgname = 'slide3.jpg';
									}
									$i = 0;
							?>												
									<li data-transition="zoomout" data-slotamount="default"  data-easein="easeInOut" data-easeout="easeInOut" data-masterspeed="2000" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7">
										<img src="/assets/uploadedimages/<?php echo $val->imgname;?>" alt="slider333" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
										<div class="tp-caption background-block NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-layer-1" 
											data-x="['left','left','left','center']" data-hoffset="['375','140','70','0']" 
											data-y="['top','top','top','top']"  data-voffset="['530','340','280','80']" 
											data-fontsize="['48','48','40','32']"
											data-lineheight="['60','60','52','52']"
											data-width="['561','561','561','400']"
											data-height="['221','221','221','200']"
											data-whitespace="nowrap"
											data-transform_idle="o:1;"
											data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
											data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
											data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
											data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
											data-start="1000" 
											data-splitin="none" 
											data-splitout="none" 
											data-responsive_offset="on"
											data-elementdelay="0.05"
											data-paddingtop="[55,55,55,55]"
											data-paddingright="[45,45,45,65]"
											data-paddingbottom="[55,55,55,55]"
											data-paddingleft="[45,45,45,25]"
											style="z-index:3; position:relative; text-transform:uppercase; width: 561px; color:#4c4c4c; height: 221px; font-family: 'Lato', sans-serif; font-weight: 300;">
											<span style="color:#FFF;">
												<?php echo $val->content02;?>
											</span>
										</div>								
									</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</div>
			
			
			<div class="welcome-section other-services container-fluid no-left-padding no-right-padding">
				<div class="container">
					<div class="row">
						
							<?php /*<form action="/bookappoinment" method="POST" autocomplete="off" class="appoinment-form" >*/?>
							<form action="/sendgrid/" method="POST" autocomplete="off" class="appoinment-form" id="aptfrom" >
								{{ csrf_field() }}							
								<h5><i class="fa fa-calendar-check-o"></i>Make Appointment</h5>
								<div class="form-group col-md-6 zno-padding">							
									<input type="text" class="form-control" placeholder="First Name" id="patient_name" name="patient_name" requried  autocomplete="off" >
									<span id="fnametxt" style="color:#F00;"></span>
								</div>
								<div class="form-group col-md-6 zno-padding">							
									<input type="text" class="form-control" placeholder="Last Name" id="patient_lname" name="patient_lname" requried  autocomplete="off">
									<span id="lnametxt" style="color:#F00;"></span>
								</div>
								<div class="form-group col-md-12 zno-padding">							
									<input type="number" class="form-control" placeholder="Phone" id="patient_contact" name="patient_contact" requried autocomplete="off">
									<span id="contacttxt" style="color:#F00;"></span>
								</div>
								<div class="form-group col-md-12 zno-padding">							
									<input type="text" class="form-control" placeholder="Email" id="patient_emailid" name="patient_emailid" requried  autocomplete="off">
									<span id="emailtxt" style="color:#F00;"></span>
								</div>
								<div class="form-group col-md-6 zno-padding">							
									Contact Preference
									<input type="radio" name="contact_preference" value="phone" checked /> Phone  
									<input type="radio" name="contact_preference" value="email" /> Email 
									<?php /*
									<div class="col-sm-7 col-md-7">
										<div class="input-group">
											<div id="radioBtn" class="btn-group">
												<a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="Y">YES</a>
												<a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="N">NO</a>
											</div>
											<input type="hidden" name="happy" id="happy">
										</div>
									</div>*/ ?>
								</div>								
								
								<div class="form-group col-md-6 zno-padding">							
									Are you new patient : 
									<input type="radio" name="new_patient" value="yes" checked /> Yes  
									<input type="radio" name="new_patient" value="no" /> No 
								</div>
								<div class="form-group input-group col-md-12 zno-padding">
									<div class="col-md-12 no-padding">
										<div class="col-md-6 col-sm-6 col-xs-6 zno-left-padding">
											<div class='date' id='datetimepicker1'>
												<input type='text' class="form-control" id="patient_date" name="patient_date" placeholder="<?php echo date('m/d/Y',strtotime('+1 day'));?>" requried />
												<?php /*<span class="fa fa-angle-down"></span>*/ ?>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 zno-left-padding">
											<select class="selectpicker" id="patient_time" name="patient_time" requried>
												<option>Time</option>
												<option  selected>09:00</option>
												<option>10:00</option>
												<option>11:00</option>
												<option>12:00</option>
												<option>13:00</option>
												<option>14:00</option>
												<option>15:00</option>
												<option>16:00</option>
												<option>17:00</option>
												<option>18:00</option>
												<option>19:00</option>
											</select>
										</div>
									</div>
								</div>
								
								<div class="form-group col-md-12 zno-padding">							
									Are you using Insurance ? : 
									<input type="radio" name="insurance" value="yes" checked /> Yes
									<input type="radio" name="insurance" value="no" /> No
								</div>
								
								<div class="form-group col-md-12 col-sm-12 col-xs-12 zno-padding">							
									<textarea rows="4" class="form-control" placeholder="Insurance information ( Optional )" id="patient_message" name="patient_message" requried></textarea>
								</div>
								<input type="button" name="sbmt" value="Submit" class="btn-submit pull-right"  onclick="frmsbmt()" />
								
								<?php if(isset($_REQUEST['msg']) && $_REQUEST['msg'] != ''){echo base64_decode($_REQUEST['msg']);}?>
								<?php /*<button type="submit" id="appointment_submit" class="btn-submit pull-right" onclick="sbmt()">
								<i class="fa fa-heart-o" onclick="sbmt()"></i>Submit</button>*/?>
								<div id="appointment-alert-msg" class="alert-msg" >
								</div>
							</form>
<script>
function frmsbmt(){
	var patient_name 	= $('#patient_name').val();
	var patient_lname	= $('#patient_lname').val();
	var patient_contact	= $('#patient_contact').val();//
	var patient_emailid	= $('#patient_emailid').val();


		$('#fnametxt').html('');
		$('#lnametxt').html('');
		$('#contacttxt').html('');
		$('#emailtxt').html('');








	
	
	
	if(patient_name == ''){
		$('#patient_name').focus();//
		$('#fnametxt').html('Please enter patient name.');
		return false;
	}
	if(patient_lname == ''){
		$('#patient_lname').focus();//
		$('#lnametxt').html('Please enter patient last name.');
		return false;
	}
	if(patient_contact == ''){
		$('#patient_contact').focus();//
		$('#contacttxt').html('Please enter contact number.');
		return false;
	}
	if(patient_emailid == ''){
		$('#patient_emailid').focus();//
		$('#emailtxt').html('Please enter email id.');
		return false;
	}
	
	$('#aptfrom').submit();
}
</script>

					</div>
				</div>
			</div>
		




		
			<div class="welcome-section other-services container-fluid no-left-padding no-right-padding" style="padding-top:10px;">
				<div class="container">
					<div class="section-header">
						<h3><?php echo $homepagecms[0]->content01;?></h3>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 our-clinic">
							<div class="row welcome-left">
							
			<div class="department-section container-fluid no-left-padding no-right-padding" style="padding-top:0px; padding-bottom:5px;">
				<div class="container1">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6 department-box" style="width:98%; text-align:left;">
							<?php echo $homepagecms[0]->content02;?>
						</div>
					</div>
				</div>
			</div>
								
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 clinic-form">
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Welcome Section /- -->
						

		
			<div class="welcome-section other-services container-fluid no-left-padding no-right-padding" style="padding-top:10px;">
				<div class="container">
					<div class="section-header">
						<h3><?php echo $homepagecms01[0]->content01;?></h3>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 our-clinic">
							<div class="row welcome-left">
							
			<div class="department-section container-fluid no-left-padding no-right-padding" style="padding-top:0px;">
				<div class="container1">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6 department-box" style="width:98%; text-align:left;">
							<?php echo $homepagecms01[0]->content02;?>
						</div>
					</div>
				</div>
			</div>
								
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 clinic-form">
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Welcome Section /- -->
						
			
			<div class="testimonial-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Testimonials</h3>
					</div><!-- Section Header /- -->
				</div>
				<div class="testimonial-slider">
					<?php
						$i = 0;
						while($i<3){$i++;
						foreach($testimonials as $val){
					?>
					<div class="testimonial-box">
						<div class="testimonial-content">
							<i><img src="assets/uploadedimages/<?php echo $val->imgname;?>" alt="testimonial"/></i>
							<?php 
								echo '<h5>'.$val->content01.'</h5>';
								echo substr($val->content02,0,250);
								if(strlen($val->content02)>250){
									echo '<span style="color:#1609de; font-size:15px;" onclick="readmore('.$val->id.')" id="rmtest'.$val->id.'">&nbsp; &nbsp; Read More</span>';
									echo '<span style="display:none;" id="test'.$val->id.'">'.substr($val->content02,250).'</span>';
								}
							?>
						</div>
					</div>
					<?php
						}
						}
					?>
					
				</div>
			</div><!-- Testimonial Section /- -->
			
			<div id="team-section1" class="team-section container-fluid no-left-padding no-right-padding">
				<div class="container">
					<div class="section-header">
						<h3>Location</h3>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3361.5449141159497!2d-96.93812778482267!3d32.591658781028364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e8d9376058d1d%3A0xce0f7030d014004!2s445+FM1382+%236%2C+Cedar+Hill%2C+TX+75104%2C+USA!5e0!3m2!1sen!2sin!4v1562171221600!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			
			
			
<style>


.department-section .department-img-block span{
	font-size:14px;
}
#radioBtn .notActive{
    color: #3276b1;
    background-color: #00D2EB;
}
</style>

<script>
function readmore(id){
	$('#rmtest'+id).hide();
	$('#test'+id).show();
}


sbmt = function(){
	//alert('dddddddd');
}
$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})
</script>		
		
		@endsection