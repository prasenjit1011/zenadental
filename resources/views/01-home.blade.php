@extends('layouts.template')
@section('content')
			<!-- Slider Section -->
			<div id="home-revslider" class="slider-section container-fluid no-padding">
				<!-- START REVOLUTION SLIDER 5.0 -->
				<div class="rev_slider_wrapper">
					<div id="home-slider1" class="rev_slider" data-version="5.3">
						<ul>
							<?php
								foreach($banner as $val){
									//print_r($val->imgname);
							?>												
							<li data-transition="zoomout" data-slotamount="default"  data-easein="easeInOut" data-easeout="easeInOut" data-masterspeed="2000" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7">
								<img src="/assets/uploadedimages/<?php echo $val->imgname;?>" alt="slider333" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
								<div class="tp-caption background-block NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-layer-1" 
									data-x="['left','left','left','center']" data-hoffset="['375','140','70','0']" 
									data-y="['top','top','top','top']"  data-voffset="['530','340','280','80']" 
									data-fontsize="['48','48','40','32']"
									data-lineheight="['60','60','52','52']"
									data-width="['561','561','561','400']"
									data-height="['221','221','221','200']"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
									data-start="1000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on"
									data-elementdelay="0.05"
									data-paddingtop="[55,55,55,55]"
									data-paddingright="[45,45,45,65]"
									data-paddingbottom="[55,55,55,55]"
									data-paddingleft="[45,45,45,25]"
									style="z-index:3; position:relative; text-transform:uppercase; width: 561px; color:#4c4c4c; height: 221px; font-family: 'Lato', sans-serif; font-weight: 300;">
									<span class="title-txt" style="font-size: 48px; font-weight:bold;">Medical</span> Services <br> You Can <span style="font-weight:bold; font-size: 48px;">Trust</span>
								</div>
								
								<div class="tp-caption NotGeneric-Button rev-btn learn-btn rs-parallaxlevel-0" id="slide-layer-2" 
									data-x="['left','left','left','center']" data-hoffset="['425','190','110','-20']" 
									data-y="['top','top','top','top']"  data-voffset="['725','535','475','250']" 
									data-fontsize="['18','18','18','18']"
									data-width="['224','224','224','200']"
									data-height="['86','86','86','50']"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:[105%];s:1000;e:Power4.slideInLeft;" 
									data-transform_out="y:[100%];s:1000;s:1000;e:Power2.slideInLeft;"
									data-start="3000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									data-responsive="off"
									data-paddingtop="[15,15,15,15]"
									data-paddingright="[10,10,10,10]"
									data-paddingbottom="[15,15,15,70]"
									data-paddingleft="[10,10,10,10]"
									style="z-index: 10; border-radius: 0; letter-spacing:0.8px; color: #fff; font-family: 'Poppins', sans-serif; text-transform:capitalize; white-space:nowrap; outline:none; box-shadow:none; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;"><span class="button-txt"><i class="fa fa-stethoscope" style="text-align: center; line-height: 50px; width: 55px; height: 55px; font-size: 24px; border-radius: 50%; background-color: #fff; vertical-align: middle; box-shadow: inset 0px 5px 5px 0px rgba(0, 0, 0, 0.06); margin-right: 10px;"></i> Learn More</span>
								</div>
							</li>
							<?php
								}
							?>
							
						</ul>
					</div>
				</div><!-- END OF SLIDER WRAPPER -->
			</div><!-- Slider Section /- -->
			
			<!-- Welcome Section -->
			<div class="welcome-section other-services container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Welcome to Our Clinic</h3>
					</div><!-- Section Header /- -->
					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12 our-clinic">
							<div class="row welcome-left">
								<?php
									if(false)
									foreach($ourclinic as $val)
									{
								?>						
						
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="other-services-block">
										<div class="services-block-icon">
											<i class="fa fa-ambulance"></i>
										</div>
										<div class="other-services-content">
											<h5>Emergency services</h5>
											<p>Dolor sit amet consecdi pisicing eliamsed do eiusmod tempornu</p>
										</div>
									</div>
								</div>
								<?php
									}
								?>
								
								
								

					
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/RootCanal.png" alt="dept">
								<span>Root Canal</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalCheckup.png" alt="dept">
								<span>Dental Checkup</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalProtection.png" alt="dept">
								<span>Dental Protection</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/OralCavity.png" alt="dept">
								<span>Oral Cavity</span>
							</div>
						</div>
						
						
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/ToothXray.png" alt="dept">
								<span>Tooth Xray</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/ToothDetail.png" alt="dept">
								<span>Tooth Detail</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalCalculus.png" alt="dept">
								<span>Dental Calculus</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalService.png" alt="dept">
								<span>Dental Service</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalBraces.png" alt="dept">
								<span>Dental Braces</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/TeethWhitening.png" alt="dept">
								<span>Teeth Whitening</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/Scaling.png" alt="dept">
								<span>Scaling</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/ToothBonding.png" alt="dept">
								<span>Tooth Bonding</span>
							</div>
						</div>
								
								
								
								
					
					
					
						<?php
						if(FALSE)
						foreach($departments as $val){
						?>
							<div class="col-md-3 col-sm-6 col-xs-6 department-box">
								<div class="department-img-block">
									<img src="assets/uploadedimages/<?php echo $val->imgname;?>" alt="dept">
									<span><?php echo $val->image_content;?></span>
								</div>
							</div>
						<?php
						}
						?>
														
								
								
								
								
								
							</div>
							
							
							
							
							
							
							
							
							
							
							
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 clinic-form">
							<form class="appoinment-form">
								<h5><i class="fa fa-calendar-check-o"></i>Make Appointment</h5>
								<div class="form-group col-md-12 no-padding">							
									<input type="text" class="form-control" placeholder="Your Name" id="patient_name" name="patient-name">
								</div>
								<div class="form-group col-md-12 no-padding">							
									<input type="email" class="form-control" placeholder="Email Address" id="patient_email" name="patient-email">
								</div>
								<div class="form-group input-group col-md-12 no-padding">
									<div class="col-md-7 col-sm-8 col-xs-8 no-padding">
										<div class="col-md-6 col-sm-6 col-xs-6 no-left-padding">
											<div class='date' id='datetimepicker1'>
												<input type='text' class="form-control" id="patient_date" name="patient-date" placeholder="Day" />
												<span class="fa fa-angle-down"></span>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 no-left-padding">
											<select class="selectpicker" id="patient_time" name="patient-time">
												<option>Time</option>
												<option>09:00</option>
												<option>10:00</option>
												<option>11:00</option>
												<option>12:00</option>
												<option>13:00</option>
												<option>14:00</option>
												<option>15:00</option>
												<option>16:00</option>
												<option>17:00</option>
												<option>18:00</option>
												<option>19:00</option>
											</select>
										</div>
									</div>
									<div class="col-md-5 col-sm-4 col-xs-4 no-padding">
										<select class="selectpicker"  id="patient_department" name="patient-department">
											<option>Department</option>
											<option>Department 1</option>
											<option>Department 2</option>
											<option>Department 3</option>
											<option>Department 4</option>
											<option>Department 5</option>
										</select>
									</div>
								</div>
								<div class="form-group col-md-12 col-sm-12 col-xs-12 no-padding">							
									<textarea rows="4" class="form-control" placeholder="Your Message..." id="patient_message" name="patient-message"></textarea>
								</div>
								<button type="submit" id="appointment_submit" class="btn-submit pull-right"><i class="fa fa-heart-o"></i>Submit</button>
								<div id="appointment-alert-msg" class="alert-msg"></div>
							</form>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Welcome Section /- -->
			
			<!-- Gallery Section -->
			<div id="gallery-section" class="gallery-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Our Gallery</h3>
					</div>
					<?php /*<ul id="filters" class="portfolio-categories no-left-padding">
						<li><a data-filter="*" class="active" href="#">Show All</a></li>
						<li><a data-filter=".design" href="#">Denthal</a></li>
						<li><a data-filter=".video" href="#">GastroEntorology</a></li>
						<li><a data-filter=".photography" href="#">Surgeries</a></li>
						<li><a data-filter=".web" href="#">Cardiology</a></li>
						<li><a data-filter=".music" href="#">Patology</a></li>
					</ul>*/ ?>
					<ul class="portfolio-list no-left-padding">
						<?php
							$i = 0;
							//foreach($gallery as $val){
							//print_r($val->imgname);
							while($i<12){
								$i++;
						?>						
						<li class="col-md-4 col-sm-4 col-xs-6 design">
							<div class="content-image-block">
								<img src="assets/uploadedimages/g<?php echo $i;?>.jpg" alt="gallery">
								<div class="content-block-hover">
									<span><?php echo $val->image_content;?></span>
									<h5>Happy patient</h5>
									<a href="#"><i class="fa fa-heart-o"></i>80</a>
									<a class="zoom-in" href="assets/images/gallery-1.jpg"><i class="fa fa-expand"></i></a>
									<a href="gallery-single.html"><i class="fa fa-file-text-o"></i></a>
								</div>
							</div>
						</li>
						<?php
							}
						?>
					</ul>
				</div><!-- Container /- -->
			</div><!-- Gallery Section -->
			
			<!-- Services Section -->
			<div class="services-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<div class="col-md-7 col-sm-12 col-xs-12">
						<div class="service-content">
							<h5>Where Your Dental Health Comes First</h5><br>
							<p>Achieve optimal dental health when you turn to the dentists at Zena Dental PLLC in Cedar Hill, TX for your oral care needs. A premier dental clinic owned by Dr. Zena Ehondor, we offer a variety of dental health services ranging from oral prophylaxis and tooth extraction to cosmetic dentistry and dental implant installation. Rest assured that when you turn to us for your dental health needs, we will go above and beyond to restore and maintain the beauty of your pearly whites.</p>
							<?php /*<div class="medical-services">
								<div class="col-md-4 col-sm-4 col-xs-4 medical-box">
									<i class="fa fa-calendar-plus-o"></i>
									<p>Easy Appointments</p>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 medical-box">
									<i class="fa fa-ambulance"></i>
									<p>Transportation</p>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 medical-box">
									<i class="fa fa-thumbs-up"></i>
									<p>Care management</p>
								</div>
							</div>
							<a href="#" class="learn-more" title="Learn More">Learn More</a>
							<span>Or</span>
							<a href="#" class="learn-more appointment" title="Make an Appointment">Make an Appointment</a>
							*/?>
						</div>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 services-img">
						<?php /*<img src="assets/uploadedimages/<?php echo $services[0]->imgname;?>" alt="services" />*/ ?>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3361.5449141159497!2d-96.93812778482267!3d32.591658781028364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e8d9376058d1d%3A0xce0f7030d014004!2s445+FM1382+%236%2C+Cedar+Hill%2C+TX+75104%2C+USA!5e0!3m2!1sen!2sin!4v1562171221600!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div><!-- Container /- -->
			</div><!-- Services Section -->
			
			<!-- Testimonial Section -->
			<div class="testimonial-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Testimonials</h3>
					</div><!-- Section Header /- -->
				</div>
				<div class="testimonial-slider">
					<?php
						$i = 0;
						while($i<3){$i++;
						foreach($testimonials as $val){
					?>
					<div class="testimonial-box">
						<div class="testimonial-content">
							<i><img src="assets/uploadedimages/<?php echo $val->imgname;?>" alt="testimonial"/></i>
							<?php echo $val->image_content;?>
						</div>
					</div>
					<?php
						}
						}
					?>
					
				</div>
			</div><!-- Testimonial Section /- -->
			
			<!-- Team Section -->
			<div id="team-section" class="team-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Doctors</h3>
					</div>
					<div class="team-carousel">
						<?php
						foreach($doctors as $val){
						?>
						<div class="col-md-12">
							<div class="team-content">
								<div class="team-box">
									<img src="assets/uploadedimages/<?php echo $val->imgname;?>" alt="team" />
									<h5><?php echo $val->image_content;?></h5>
								</div>
								<span class="team-catagory"><?php echo $val->content01;?></span>
							</div>
						</div>
						<?php
						}
						?>
						
					</div>
				</div><!-- Container /- -->
			</div><!-- Team Section /- -->
			
			<!-- Latest News -->
			<div class="latest-news container-fluid no-left-padding no-right-padding" style="display:none;">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Our News</h3>
					</div><!-- Section Header /- -->
					<div class="row">
						<div class="news-carousel">
							<div class="col-md-12">
								<div class="type-post">
									<div class="entry-cover">
										<a title="Blog" href="blog-post.html">
											<img alt="blog" src="assets/images/blog-1.jpg" />
										</a>
										<div class="post-date-bg">
											<div class="post-date">
												18 <span>June</span>
											</div>
										</div>
									</div>
									<div class="latest-news-content">
										<div class="entry-header">
											<h3 class="entry-title"><a title="Patients Share Success Stories" href="blog-post.html">Patients Share Success Stories</a></h3>
											<div class="entry-meta">
												<div class="byline"><a href="#" title="adminol"><i class="fa fa-user-o"></i>by adminol</a></div>
												<div class="post-time"><a href="#" title="10 minutes ago"><i class="fa fa-clock-o"></i>10 minutes ago</a></div>
												<div class="post-comment"><a href="#" title="4 Comments"><i class="fa fa-commenting-o"></i>4 Comments</a></div>
											</div>
										</div>
										<div class="entry-content">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard .</p>
										</div>
										<a href="blog-post.html" title="Read More" class="read-more">Read More</a>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="type-post">
									<div class="entry-cover">
										<a title="Blog" href="blog-post.html">
											<img alt="blog" src="assets/images/blog-2.jpg" />
										</a>
										<div class="post-date-bg">
											<div class="post-date">
												18 <span>June</span>
											</div>
										</div>
									</div>
									<div class="latest-news-content">
										<div class="entry-header">
											<h3 class="entry-title"><a title="Broad range of specialist services" href="blog-post.html">Broad range of specialist services</a></h3>
											<div class="entry-meta">
												<div class="byline"><a href="#" title="adminol"><i class="fa fa-user-o"></i>by adminol</a></div>
												<div class="post-time"><a href="#" title="10 minutes ago"><i class="fa fa-clock-o"></i>10 minutes ago</a></div>
												<div class="post-comment"><a href="#" title="4 Comments"><i class="fa fa-commenting-o"></i>4 Comments</a></div>
											</div>
										</div>
										<div class="entry-content">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard .</p>
										</div>
										<a href="blog-post.html" title="Read More" class="read-more">Read More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Latest News /- -->
			
			<!-- Offer Section -->
			<div class="offer-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>What We Offer</h3>
					</div><!-- Section Header /- -->
					<div class="row">
						<?php
						
						?>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box">
								<i class="fa fa-hospital-o"></i>
								<h5>Intensive Care</h5>
								<p>Mister we could use a man like Herbert Hoover again. Love exciting and new. Come aboardwere expecting you.</p>
								<a href="#" title="Read More">Read More</a>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box">
								<i class="fa fa-ambulance"></i>
								<h5>24/7 Ambulance</h5>
								<p>In crack commando was sent to prison by a military court for a crime they didn't commit. These men prompt-</p>
								<a href="#" title="Read More">Read More</a>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box">
								<i class="fa fa-smile-o"></i>
								<h5>Friendly Doctors</h5>
								<p>Come aboard expecting you. Love life's sweetest reward Let it flow it floats back to you. It's a beautiful Day.</p>
								<a href="#" title="Read More">Read More</a>
							</div>
						</div>
					</div>
				</div><!-- Container -->
			</div><!-- Offer Section /- -->
			<?php /*
			<!-- Department Section -->
			<div class="department-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<div class="department-header">
						<h5>Departments</h5>
					</div>
					
					
					
					<div class="show-all">
						<a href="#" title="show all">show all</a>
					</div>
				</div><!-- Container /- -->
			</div><!-- Department Section -->*/ ?>
		@endsection