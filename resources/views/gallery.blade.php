@extends('layouts.template')
@section('content')
						
		<!-- Page Banner -->
			<div class="page-banner container-fluid no-left-padding no-right-padding" style="background-image:url('/assets/uploadedimages/g11.jpg')">
				<!-- Container -->
				<div class="container">
					<div class="page-banner-content">
						<h3> </h3>
					</div>
					<div class="banner-content">
						<ol class="breadcrumb">
							<li><a href="index.html">Home</a></li>
							<li class="active">Gallery</li>
						</ol>
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Banner -->
			
			
			<div id="gallery-section" class="gallery-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Our Gallery</h3>
					</div>
					<ul class="portfolio-list no-left-padding">
						<?php
							$i = 0;
							while($i<18){
								$i++;
						?>						
						<li class="col-md-4 col-sm-4 col-xs-6 design">
							<div class="content-image-block">
								<img src="/assets/uploadedimages/gi<?php echo $i;?>.jpg" alt="gallery">
								<div class="content-block-hover">
									<span>Dental Services You Can Trust</span>
									<h5>Happy patient</h5>
									<a href="#"><i class="fa fa-heart-o"></i></a>
									<a class="zoom-in" href="/assets/uploadedimages/gi<?php echo $i;?>.jpg"><i class="fa fa-expand"></i></a>
									<a href="gallery-single.html"><i class="fa fa-file-text-o"></i></a>
								</div>
							</div>
						</li>
						<?php
							}
						?>
					</ul>
				</div><!-- Container /- -->
			</div><!-- Gallery Section -->
			
			
<!-- Offer Section -->
			<div class="offer-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3 style="color:#000;">Service Offerings </h3>
					</div><!-- Section Header /- -->
					<div class="row">
						<?php
						
						?>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box" style="initial">
								<h5 style="center">General Dentistry </h5>
								<p style="text-align:initial;">We offer a wide range of general dental treatments, which include:</p>
								<p style="text-align:initial;">Dental Cleaning and Tooth Scaling</p>
								<p style="text-align:initial;">Tooth Restorations - Amalgam Fillings (silver) or Composite Fillings (white)</p>
								<p style="text-align:initial;">Permanent Crowns/Bridges, Dental Implants, Porcelain Veneers</p>
								<p style="text-align:initial;">Root Canal</p>
								<p style="text-align:initial;">Surgical Extractions</p>
								<p style="text-align:initial;">Digital Radiography</p>
								<p style="text-align:initial;">Dental Implants</p>
								<p style="text-align:initial;">Full/ Partial Dentures (Flexible or metal)</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box" style="initial">
								<h5 style="center">Cosmetic Dentistry</h5>
								<p style="text-align:initial;">Cosmetic Dentistry is the process of improving the aesthetic quality of a person’s smile.</p>
								<p style="text-align:initial;">A purely cosmetic procedure is one that is performed without a medical reason, solely to improve a patient’s appearance. Through cosmetic dentistry, we are able to redesign the functional aspects of the teeth so that they have a unified, uniform, natural look. We achieve this by using special non-metal materials, such as porcelain or composites, which allow        for a more natural looking smile.</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box" style="initial">
								<h5 style="center">Orthodontics DENTISTRY</h5>
<p style="text-align:initial;">The best and most conservative way to improve your smile and the health of your teeth is through orthodontic treatment. With either conventional orthodontics (clear or metal braces) or Invisalign treatment (clear removable trays), Zena dental will create for you a beautiful and natural smile. We specialize in orthodontics for patients of all ages.</p>
<p style="text-align:initial;">Dental Smile Makeovers</p>
<p style="text-align:initial;">Clear and Metal Braces</p>
<p style="text-align:initial;">Invisible Braces and Invisalign</p>
<p style="text-align:initial;">Invisalign Teen Braces</p>
<p style="text-align:initial;">Arrowlign / Fix Six</p>
							</div>
						</div>
					</div>
				</div><!-- Container -->
			</div><!-- Offer Section /-  -->
					
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			<?php if(0){?>
			<!-- Department Section -->
			<div class="department-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<div class="department-header">
						<h5>Departments</h5>
					</div>
					
					<div class="row">

						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/RootCanal.png" alt="dept">
								<span>Root Canal</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalCheckup.png" alt="dept">
								<span>Dental Checkup</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalProtection.png" alt="dept">
								<span>Dental Protection</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/OralCavity.png" alt="dept">
								<span>Oral Cavity</span>
							</div>
						</div>
						
						
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/ToothXray.png" alt="dept">
								<span>Tooth Xray</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/ToothDetail.png" alt="dept">
								<span>Tooth Detail</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalCalculus.png" alt="dept">
								<span>Dental Calculus</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalService.png" alt="dept">
								<span>Dental Service</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/DentalBraces.png" alt="dept">
								<span>Dental Braces</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/TeethWhitening.png" alt="dept">
								<span>Teeth Whitening</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/Scaling.png" alt="dept">
								<span>Scaling</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 department-box">
							<div class="department-img-block">
								<img src="assets/uploadedimages/teeth/ToothBonding.png" alt="dept">
								<span>Tooth Bonding</span>
							</div>
						</div>
													
					</div>
					
					
				</div><!-- Container /- -->
			</div><!-- Department Section -->
			<?php }	?>
<style>
.department-section .department-img-block span{
	font-size:14px;
}

</style>

<script>
sbmt = function(){
	//alert('dddddddd');
}
</script>		
		
		@endsection