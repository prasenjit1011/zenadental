<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

	<title>Zena Dental PLLC</title>

	<!-- Standard Favicon -->
	<link rel="icon" type="image/x-icon" href="assets/images//favicon.ico" />
	
	<!-- For iPhone 4 Retina display: -->
	<link rel="apple-touch-icon-precomposed" href="assets/images//apple-touch-icon-114x114-precomposed.png">
	
	<!-- For iPad: -->
	<link rel="apple-touch-icon-precomposed" href="assets/images//apple-touch-icon-72x72-precomposed.png">
	
	<!-- For iPhone: -->
	<link rel="apple-touch-icon-precomposed" href="assets/images//apple-touch-icon-57x57-precomposed.png">	
	
	<!-- Library - Google Font Familys -->
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Montserrat+Alternates:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Playfair+Display:400,400i,700,700i,900,900i|Poppins:300,400,500,600,700|Quattrocento:400,700|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	
	<?php if($pagename	== 'sericespage123'){?>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<?php } ?>
	<link rel="stylesheet" type="text/css" href="assets/revolution/css/settings.css">
	
	<link rel="stylesheet" type="text/css" href="assets/revolution/fonts/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="assets/revolution/fonts/font-awesome/css/font-awesome.min.css">
	
	<!-- RS5.0 Layers and Navigation Styles -->
	<link rel="stylesheet" type="text/css" href="assets/revolution/css/layers.css">
	<link rel="stylesheet" type="text/css" href="assets/revolution/css/navigation.css">
	
	<!-- Library - Bootstrap v3.3.5 -->
    <link href="assets/css/lib.css" rel="stylesheet">
	
	<link href="assets/js/slick/slick.css" rel="stylesheet">	
    <link href="assets/js/slick/slick-theme.css" rel="stylesheet">

	<!-- Custom - Common CSS -->
	<link href="assets/css/plugins.css" rel="stylesheet">
	<?php /*<link href="assets/css/elements.css" rel="stylesheet">*/?>
	<link href="assets/css/elements08.css" rel="stylesheet">
	<link href="assets/css/rtl.css" rel="stylesheet">
	<link id="color" href="assets/css/color-schemes/default.css" rel="stylesheet"/>
	<!-- Custom - Theme CSS -->
	<link rel="stylesheet" type="text/css" href="style.css">
	
	<!--[if lt IE 9]>
		<script src="js/html5/respond.min.js"></script>
    <![endif]-->
	
</head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">
<div class="boxed-container">
	<!-- Loader -->
	<!--div id="site-loader" class="load-complete">
		<div class="loader">
			<div class="loader-inner ball-clip-rotate">
				<div></div>
			</div>
		</div>
	</div--><!-- Loader /- -->		
		
	<!-- Style Switcher -->
	<div class="style-switcher" id="choose_style" style="display:none;">
		<a href="#." class="picker_close">
			<i class="fa fa-gear fa-spin"></i>
		</a>
		<div class="style-picker">
			<h3>Select Your Style</h3>
			<div class="choose-switcher-box">
				<p>Choose View Style</p>
				<ul>
					<li><a href="#." class="full-view" id="full_view"><img src="assets/images/full.png" alt="Full" /></a></li>
					<li><a href="#." class="default-view" id="box_view"><img src="assets/images/boxed.png" alt="Boxed" /></a></li>
				</ul>
			</div>
			<div class="color-switcher-block">
				<p>Choose Colour style</p>
				<ul>
					<li><a href="#." class="skyblue" id="default"><img src="assets/images/skyblue.jpg" alt="skyblue"/></a></li>
					<li><a href="#." class="green" id="green"><img src="assets/images/green.jpg" alt="green"/></a></li>
					<li><a href="#." class="blue" id="blue"><img src="assets/images/blue.jpg" alt="blue"/></a></li>
					<li><a href="#." class="coral" id="coral"><img src="assets/images/coral.jpg" alt="coral"/></a></li>
					<li><a href="#." class="cyan" id="cyan"><img src="assets/images/cyan.jpg" alt="cyan"/></a></li>
					<li><a href="#." class="eggplant" id="eggplant"><img src="assets/images/eggplant.jpg" alt="eggplant"/></a></li>
					<li><a href="#." class="pink" id="pink"><img src="assets/images/pink.jpg" alt="pink"/></a></li>
					<li><a href="#." class="slateblue" id="slateblue"><img src="assets/images/slateblue.jpg" alt="slateblue"/></a></li>
					<li><a href="#." class="gold" id="gold"><img src="assets/images/gold.jpg" alt="gold"/></a></li>
					<li><a href="#." class="red" id="red"><img src="assets/images/red.jpg" alt="red"/></a></li>
				</ul>
			</div>
		</div>
	</div><!-- Style Switcher /- -->	

	<!-- Header Default -->
	<header class="header_s header_default">
		<!-- Container -->
		<div class="container">
			<!-- Top Header -->
			<div class="default-top row">
				<div class="logo-block">
					<a class="navbar-brand mobile-hide" href="/">
						<img src="/assets/uploadedimages/zena-dental-pllc.png" />
					</a>
				</div>
				<a href="tel:4692720003" title="+(469) 272-0003" class="phone-call"><i class="fa fa-phone-square"></i> (469) 272-0003</a>
			</div><!-- Top Header /- -->
		</div><!-- Container /- -->
		<!-- Ownavigation -->
		<nav class="navbar ownavigation nav_absolute">
			<!-- Container -->
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
					<a class="navbar-brand desktop-hide" href="#"><img src="/assets/uploadedimages/zena-dental-pllc.png" /></a>
				</div>
				<div class="navbar-collapse collapse" id="navbar">
					<ul class="nav navbar-nav menubar">
						<li <?php if($pagename	== 'homepage'){?> class="active"<?php } ?>><a title="Home" href="/">Home</a></li>						
						<li <?php if($pagename	== 'aboutpage'){?> class="active"<?php } ?>><a title="About" href="/about">About</a></li>
						
						<li <?php if($pagename	== 'sericespage'){?> class="active"<?php } ?>><a title="Services" href="/services">Services</a></li>
						<li <?php if($pagename	== 'gallerypage'){?> class="active"<?php } ?>><a title="Gallery" href="/gallery">Gallery</a></li>
						<?php /*<li <?php if($pagename	== 'homepage1'){?> class="active"<?php } ?>><a title="Doctor" href="/#team-section">Our Staff </a></li>*/ ?>
						<li <?php if($pagename	== 'drzena'){?> class="active"<?php } ?>><a title="About" href="/our-staff">Our Staff</a></li>
						<li <?php if($pagename	== 'contactpage'){?> class="active"<?php } ?>><a title="Work" href="/contact"  >Contact</a></li>						
					</ul>
				</div>
			</div><!-- Container /- -->
		</nav><!-- Ownavigation /- -->
	</header><!-- Header Default /- --> 
		
	<div class="main-container">
		<main>
			@yield('content')
    	</main>
	</div>



<a style="display:none;" id="mailsent" href="#" data-toggle="modal" data-target="#addedit">Test</a>
<div class="modal fade" id="addedit" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>				
				<div class="modal-body">
				<h3>Appointment Scheduled Successfully!<h3>
<?php 
	if(isset($_REQUEST['msg']) && $_REQUEST['msg'] != ''){
		echo '<h4>'.base64_decode($_REQUEST['msg']).'</h4>';
	}
?>	
				</div>
		</div>
	</div>
</div> 
    
	
	
	<!-- Footer Main -->
	<footer id="footer-main" class="footer-main container-fluid no-left-padding no-right-padding">
		<!-- Container -->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-6 contact-info">
					<aside class="widget widget_info">
						<h3 class="widget-title">Contact info</h3>
						<p><i class="fa fa-phone"></i><a href="tel:4692720003" style="cursor:pointer;" title="+(469) 272-0003">(469) 272-0003</a></p>
						<p><i class="fa fa-envelope"></i><a href="mailto:zenadental@hotmail.com" style="cursor:pointer;" title="zenadental@hotmail.com">zenadental@hotmail.com</a></p>
						<p>
							<i class="fa fa-map-marker"></i>
							445 E.FM 1382, Suite #6,<br>
							<span style="margin-left:65px;">
								Cedar Hill, TX 75104
							</span>
						</p>
						<div class="footer-social">
							<i class="fa fa-heart"></i>
							<ul>
								<li><a href="https://facebook.com/zenadental" title="Facebook"><img src="/sendgrid/facebook_circle-512.png"  style="width:34px;" /> &nbsp; &nbsp; </a></li>
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<aside class="widget widget_workinghours">
						<span><i class="fa fa-clock-o"></i></span>
						<h3 class="widget-title">Working Hours</h3>
						<ul>
							<li><span>Monday</span> <b>9.00  AM  To  6.00 PM</b></li>
							<li><span>Tuesday</span> <b>9.00  AM  To  6.00 PM	</b></li>
							<li><span>Wednesday</span> <b>9.00  AM  To  6.00 PM</b></li>
							<li><span>Thursday</span> <b>9.00  AM  To  6.00 PM</b></li>
							<li><span>Friday</span> <b>9.00  AM  To  1.00 PM</b></li>
							<li><span>Saturday</span> <b>Closed</b></li>
							<li><span>Sunday</span> <b>Closed</b></li>
						</ul>
					</aside>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6 contact-form">
					<h3 class="widget-title">Any Questions ?</h3>
					<form method="post" id="questionfrm" action="/askquestion/index.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="full Name" name="askquestion_fname" id="askquestion_fname" required="" />
						</div>
						<div class="form-group">
							<input type="text" class="form-control" placeholder="email Address" name="askquestion_email" id="askquestion_email" required="" />
						</div>
						<div class="form-group">
							<input type="text" class="form-control" placeholder="subject" name="askquestion_subject" id="askquestion_subject" />
						</div>
						<div class="form-group">
							<textarea class="form-control" placeholder="message" rows="4" name="askquestion_message" id="askquestion_message" ></textarea>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-default" style="background-color: #01d1ea; color: #FFF; font-weight: 600; float: right;;"  name="sbmt" value="Submit" />
							<button title="Learn More" id="que_btn_submit" type="submit" name="post" style="display:none;">Learn more</button>
						</div>
						<div id="alert-msg-que" class="alert-msg"></div>
					</form>
				</div>
			</div>
		</div><!-- Container /- -->
	</footer><!-- Footer Main /- -->
	
	<div class="footer-bottom">
		<!-- Container -->
		<div class="container">
			<p>&copy; <?php //echo date('Y');?> Site designed and maintained by webspoons Inc. </p>
		</div><!-- Container /- -->
	</div>
</div>
	
	<!-- JQuery v1.12.4 -->
	<script src="assets/js/jquery.min.js"></script>
	
	<!-- Library - Js -->
	<script src="assets/js/lib.js"></script>
	
	<!-- RS5.0 Core JS Files -->
	<script type="text/javascript" src="assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
	<script type="text/javascript" src="assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
	<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	
	<script src="assets/js/slick/slick.min.js"></script>
	
	<!-- Library - Google Map API -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDW40y4kdsjsz714OVTvrw7woVCpD8EbLE"></script>
	
	<!-- Library - Theme JS -->
	<script src="assets/js/functions.js"></script>
<?php 
	if(isset($_REQUEST['msg']) && $_REQUEST['msg'] != ''){
?>	
<script>
	$( document ).ready(function() {
		$('#mailsent').click();
	});
</script>	
		
<?php
	}
?>	
</body>
</html>
