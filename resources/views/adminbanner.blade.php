@extends('layouts.app')
@section('content')
<div class="page-banner container-fluid no-left-padding no-right-padding">
	<!-- Container -->
	<div class="container">
		<?php if(empty($_REQUEST['page'])){?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li class="co-md-4"><a href="/admin/appointment">Appointment Banner Management</a></li>									
				<li class="co-md-4"><a href="/admin/?page=banner">Banner Management</a></li>
				<li class="co-md-4"><a href="/admin/?page=cms">Content Management System</a></li>
				<li class="co-md-4"><a href="/admin/?page=testimonials">Testimonials Management System</a></li>
				<li class="co-md-4"><a href="/admin/?page=service">Service Management System</a></li>
				<li class="co-md-4"><a href="/admin/?page=gallery">Home Page Gallery Management</a></li>			
			</ol>
		</div>
		<?php } ?>
		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'banner'){?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li style="color: #000; font-size: large;">Banner Management System</li>
				<li style="float:right;"><a href="<?php echo url()->full().'&type=2';?>" style="text-align:right"  >Add More Banner</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						
						foreach($homebanner as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'" width="200" style="margin:10px; border:1px solid #CCC;" />
									</td> 
									<td style="padding:5px;"><h5>'.$val->content02.'<h5></td>
									<td style="padding:5px;">
										<a href="'.url()->full().'&type=2&editid='.$val->id.'">Edit</a>
									</td>
									<td style="padding:5px;">
										<a href="/admin/change/status/'.$val->id.'?type=195">'.(($val->is_active?'Active':'Inactive')).'</a>	
									</td>
								</tr>';	
								/** <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addedit" onclick="window.location.href=''">Edit</button>*/
								/** editbanner('.$val->id.',\''.$val->image_content.'\')*/
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>
		<?php } ?>
		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'cms'){?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li style="color: #000; font-size: large;">Content Management System</li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Title</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						
						foreach($content as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;"><h5>'.$val->content01.'<h5></td> 
									<td style="padding:5px;">'.$val->content02.'</td>
									<td style="padding:5px;">
										<a href="'.url()->full().'&type=2&editid='.$val->id.'">Edit</a>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>
		<?php } ?>
		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'testimonials'){?>
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li style="color: #000; font-size: large;">Testimonials Section For Home Pages</li>
				<li style="float:right;">
					<a style="" href="<?php echo url()->full().'&type=4';?>">
						Add More Testimonials
					</a>
				</li>			
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($testimonials as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="120" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;"><h4>'.$val->content01.'</h4><h5>'.$val->content02.'</h5></td>
									<td style="padding:5px;">
										<a style="color:#FFF;" href="'.url()->full().'&type=4&editid='.$val->id.'">Edit</a>
									</td>
									<td style="padding:5px;">
										<a href="/admin/change/status/'.$val->id.'?type=4">'.(($val->is_active?'Active':'Inactive')).'</a>	
									</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
				
		<br><br>
		<?php } ?>
		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'service'){?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li style="color: #000; font-size: large;">Service Offerings Management System</li>
				<li style="float:right;">
				<a href="#" style="text-align:right" data-toggle="modal" data-target="#addedit" onclick="editbanner(0,'')" >
				<a href="<?php echo url()->full().'&type=15';?>">
				Add More Service
				</a>
				</li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Title</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						
						foreach($serviceofferings as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">'.$val->content01.'</td> 
									<td style="padding:5px;"><h5>'.$val->content02.'<h5></td>
									<td style="padding:5px;">
										<a href="'.url()->full().'&type=15&editid='.$val->id.'">Edit</a>
									</td>
									<td style="padding:5px;">
										<a href="/admin/change/status/'.$val->id.'?type=15">'.(($val->is_active?'Active':'Inactive')).'</a>	
									</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>		
		
		<?php /*?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li style="color: #000; font-size: large;">Other Service Offerings Management System</li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Title</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						
						foreach($serviceofferings01 as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">'.$val->content01.'</td> 
									<td style="padding:5px;"><h5>'.$val->content02.'<h5></td>
									<td style="padding:5px;">
										<a href="'.url()->full().'&editid='.$val->id.'">Edit..</a>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>		
		<?php */
		} 
		?>
		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'gallery'){?>
		
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Gallery Image Management</a></li>
				<li style="float:right;"><a href="#" style="text-align:right" data-toggle="modal" data-target="#addedit" onclick="addgalleryimage()"  >Add More Gallery Image</a></li>			
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($gallery as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="120" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;">
										<a href="/admin/change/status/'.$val->id.'?type=3">'.(($val->is_active?'Active':'Inactive')).'</a>	
									</td>
								</tr>';	
						} 
					?>
				</tbody>
			</table>
		</div>
		
		<br><br>
		<?php } ?>
		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'others'){?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Doctors Section For Home Pages</a></li>
				<li style="float:right;"><a href="#" style="text-align:right" >Add More Doctors</a></li>			
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($doctors as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="120" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5>'.$val->content01.'</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		

		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Departments Section For Home Pages</a></li>
				<li style="float:right;"><a href="#" style="text-align:right" >Add More Departments</a></li>			
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($departments as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="80" style="margin:10px; border:1px solid #CCC; background-color:#FFF;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5></td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		


		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Clinic Section For Home Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($clinic as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5>'.$val->content01.'</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Offer Section For Home Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($offer as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5>'.$val->content01.'</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		

		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Banner Section For Other Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($pagesbanner as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="100" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5></td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		<?php } ?>
		<br><br>
		
		
		<?php /*
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Partner Section For About Us Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($partner as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="100" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>
		
		*/?>
		
	</div>
</div>


@endsection