@extends('layouts.app')
@section('content')
<div class="page-banner container-fluid no-left-padding no-right-padding">
	<!-- Container -->
	<div class="container">
		<div class="page-banner-content">
			<h3>Banner</h3>
		</div>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Gallery Section For Home Page</a></li>
				<li style="float:right;"><a href="#" style="text-align:right" >Add More Banner</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($homebanner as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'" height="150" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5></td>
									<td style="padding:5px;"><a href="#">Edit</a></td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Banner Section For Other Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($pagesbanner as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'" height="150" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5></td>
									<td style="padding:5px;"><a href="#">Edit</a></td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>
	</div>
</div>
@endsection