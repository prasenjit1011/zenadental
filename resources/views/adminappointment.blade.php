@extends('layouts.app')
@section('content')
<div class="page-banner container-fluid no-left-padding no-right-padding">
	<!-- Container -->
	<div class="container">
		

		






		
		<div class="banner-content">
			<ol class="breadcrumb">
				<li style="color: #000; font-size: large;">Appointment Management System</li>
				<li style="float:right;"><a href="#" style="text-align:right" data-toggle="modal" data-target="#addedit" onclick="editbanner(0,'')" >Add Appointment</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Contact Details</th>
						<th style="padding:5px;">Appointment Time</th>
						<th style="padding:5px;">Message</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						
						foreach($appointment as $val){
							$i++; 
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<h5>'.ucfirst($val->fname).'</h5>'.$val->contact.'
									</td> 
									<td style="padding:5px;"><h5>'.$val->sel_dtd.' '.$val->sel_time.'<h5></td>
									<td style="padding:5px;"><h5>'.$val->msg.'<h5></td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		<br><br>
	</div>
</div>


@endsection