<?php
Route::auth();
Route::get('/', ['as' => 'homepage', 'uses' => 'HomeController@index']);
Route::get('/about', ['as' => 'about', 'uses' => 'HomeController@about']);
Route::get('/services', ['as' => 'services', 'uses' => 'HomeController@services']);
Route::get('/gallery', ['as' => 'gallery', 'uses' => 'HomeController@gallery']);
Route::get('/contact', ['as' => 'contact', 'uses' => 'HomeController@contact']);
Route::get('/drzena', ['as' => 'drzena', 'uses' => 'HomeController@drzena']);
Route::get('/our-staff', ['as' => 'drzena', 'uses' => 'HomeController@drzena']);
Route::post('/bookappoinment', ['as' => 'bookappoinment', 'uses' => 'HomeController@bookappoinment']);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');




Route::group(['middleware' => 'auth'], function(){
	Route::get('/admin', ['as' => 'admin', 'uses' => 'AdminController@adminbanner']);
	Route::get('/admin/banner', ['as' => 'adminbanner', 'uses' => 'AdminController@adminbanner']);
	Route::get('/admin/gallery', ['as' => 'admingallery', 'uses' => 'AdminController@admingallery']);
	Route::get('/admin/testimonials', ['as' => 'admintestimonials', 'uses' => 'AdminController@admintestimonials']);
	Route::post('/admin/updatedata', ['as' => 'adminupdatedata', 'uses' => 'AdminController@adminupdatedata']);
	Route::get('/admin/appointment', ['as' => 'adminappointment', 'uses' => 'AdminController@adminappointment']);
	Route::get('/admin/change/status/{id}', ['as' => 'adminchangestatus', 'uses' => 'AdminController@changestatus']);
});
?>
